class Comment
  include Mongoid::Document
  include Mongoid::Timestamps

  # Fields
  field :text, type: String
  field :user, type: String  # Here :user is actually username

  # Association
  embedded_in :article, inverse_of: :comments

  # Validation
  validates_presence_of :text, :user
end
