# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'SETTING UP DEFAULT USER LOGIN'
user = User.create! :username => 'harshulj', :email => 'harshulj@gmail.com', :password => 'revenge00', :password_confirmation => 'revenge00'
puts 'New user created: ' << user.username
user2 = User.create! :username => 'devang', :email => 'devangthecool@gmail.com', :password => 'devang00', :password_confirmation => 'devang00'
puts 'New user created: ' << user2.username
user3 = User.create! :username => 'raj', :email => 'rkssisodiya@gmail.com', :password => 'rajkamal', :password_confirmation => 'rajkamal'
puts 'New user created: ' << user3.username