class Article
  include Mongoid::Document
  include Mongoid::Timestamps

  # Fields
  field :title, type: String
  field :content, type: String

  # Associations
  belongs_to :user
  embeds_many :comments

  # Validations
  validates_presence_of :title, :content, :user
end
