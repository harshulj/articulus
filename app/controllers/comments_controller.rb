class CommentsController < ApplicationController
  before_filter :authenticate_user!

  def create
	@article = Article.find(params[:article_id])
	@comment = @article.comments.create!(text: params[:comment][:text], user: current_user._id)
	redirect_to @article, notice: "Comment Created"
  end
end